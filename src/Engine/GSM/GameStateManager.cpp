// ----------------------------------------------------------------------------
// Project Name		:	Game State Manager
// File Name		:	GameStateManager.cpp
// Author			:	
// Creation Date	:	
// Purpose			:	
// ----------------------------------------------------------------------------

#include "GameStateManager.h"


// Max states in the game
#define MAX_GAME_STATES 64

// ---------------------------------------
// Globals

/*!------------------------------------------------------------------------------------------------
	\brief This structure holds the id of the gamestate and the 6 function pointers for the state.
-------------------------------------------------------------------------------------------------*/
struct GameStateInfo
{
	unsigned GameStateId;
	
	fp Load;
	fp Unload;
	fp Init;
	fp Free;
	fp Update;
	fp Render;
};

// Stores the current, previsou and next states along with the initialize state
unsigned gGameStateInit;
unsigned gGameStateCurrent;
unsigned gGameStatePrevious;
unsigned gGameStateNext;

// Game State functions
fp GameStateInit = 0;
fp GameStateLoad = 0;
fp GameStateUnload = 0;
fp GameStateFree = 0;
fp GameStateUpdate = 0;
fp GameStateRender = 0;


// An array of GameStateInfo to hold all the game state information.
GameStateInfo	gGameStates[MAX_GAME_STATES];

// The number of actual game states.
unsigned			gGameStatesCount = 0;

/*!------------------------------------------------------------------------------------------------
 \brief This is a dummy function that doesn't do anything.
 \details
 	Use this function as a substitute if one of the function pointers passed to the 
	GSM_AddGameState function is NULL.
-------------------------------------------------------------------------------------------------*/
void dummy()
{}

/*!------------------------------------------------------------------------------------------------
\brief This functions empties the game state manager. 
\details
	- In practical terms, clearing the GSM is equivalent to simply setting the gGameStatesCount 
	variable to 0. In this way, any state that is added thereafter will overwrite wha tis already
	in the game state array. 
	- Nevertheless, we are going to set all the game state values of the array to zero. To do so, 
	  a loop. 
	- Set gGameStateInit/Current/Previous/Next all to zero as well.
	- Set Game State functions (GameStateLoad/Init/Update, etc...) to zero as well.
*/
void GSM_Clear()
{
	gGameStatesCount = 0;
	for (int i = 0; i < MAX_GAME_STATES; i++) {
		gGameStates[i].Load = 0;
		gGameStates[i].Unload = 0;
		gGameStates[i].Init = 0;
		gGameStates[i].Free = 0;
		gGameStates[i].Update = 0;
		gGameStates[i].Render = 0;
		gGameStates[i].GameStateId = 0;

	}

	GameStateInit = 0;
	GameStateLoad = 0;
	GameStateUnload = 0;
	GameStateFree = 0;
	GameStateUpdate = 0;
	GameStateRender = 0;

	gGameStateInit = 0;
	gGameStateCurrent = 0;
	gGameStatePrevious = 0;
	gGameStateNext = 0;
}
/*------------------------------------------------------------------------------------------------
 \brief This function adds a state to the game state manager.
 \param gameStateIdx
 	Represents the ID of the state. This id is declared in GameStateList.h
 \param Load,Init,Update,Render,Free,Unload 
	Function pointers to the game states functions

 \details:
	The function adds a state to the GameStateManager by storing the passed arguments in the
	gGameStates array (declared above). It must first make sure that the current count of game
	states is not over the MAX_GAME_STATES macro(defined above). Then it must go through all 
	the previouly added game states and make sure that we are not adding a duplicate; if we are 
	we simply return. For example:
	
	GSM_AddGameState(GS_LEVEL1,Level1Load, Level1Init, Level1Update, Level1Render, Level1Free, Level1Unload); 
		-> GS_LEVEL1 has not been added, we add the state to the array.
	GSM_AddGameState(GS_LEVEL1,....) 
		-> This time GS_LEVEL1 exists, we don't add a duplicate, return.

	Adding a state is simply storing the gameStateIdx and all the function pointers in the gGameStates array
	of GameStateInfo structure. If one of the passed function pointer is NULL, then you MUST store
	the address of dummy. this will insure that you don't try to call a function using a NULL pointer.
	For Example:

		GSM_AddGameState(GS_LEVEL1, Level1Load, NULL, Level1Update, Level1Render, NULL, Level1Unload);
			-> Here the Init and Free function pointers are pointing to dummy.
-------------------------------------------------------------------------------------------------*/
void GSM_AddGameState(unsigned gameStateIdx, fp Load, fp Init, fp Update, fp Render, fp Free, fp Unload)
{
	gGameStates[gGameStatesCount].Load = Load;
	gGameStates[gGameStatesCount].Unload = Unload;
	gGameStates[gGameStatesCount].Init = Init;
	gGameStates[gGameStatesCount].Free = Free;
	gGameStates[gGameStatesCount].Update = Update;
	gGameStates[gGameStatesCount].Render = Render;
	gGameStates[gGameStatesCount].GameStateId = gameStateIdx;
}

/*------------------------------------------------------------------------------------------------
 \brief	This function initializes the game state manager.
 \param	gameStateInit 
 	Represents the ID of the initial state. This id is declared in GameStateList.h
	Note that the state must be added previous to the initialization.

 \details
	- Simply store this state as the initial state by storing it into gamestaeinit, next previous and current.
	- Call GSM_UpdatePtrs to change the pointers. 
	- Remember that you must write to "Output.txt" in this function. (Use the FileIOManager).
-------------------------------------------------------------------------------------------------*/
// Call this to initialize the game state manager
void GSM_Init(unsigned gameStateInit)
{
	gGameStateInit = gameStateInit;
	gGameStateCurrent = gameStateInit;
	gGameStatePrevious = gameStateInit;
	gGameStateNext = gameStateInit;
	
	
	GSM_UpdatePtrs();
}


/*!------------------------------------------------------------------------------------------------
 \brief	This function adds a state to the game state manager.
 \details	
	This function updates the function pointers of the GameStateManager. It will look up the 
	value of gGameStateCurrent in the gGameStates array. If the value is equal to the id of one 
	of the added states (see GSM_AddGameState()function above), then it sets the GameStateManager
	function pointers to the corresponding functions (again added in the GSM_AddGameState function).

	if the value of gGameStateCurrent is equal to GS_RESTART or GS_QUIT then the GSM function pointers
	fill point to the dummy function.

	if the value of gGameStateCurrent is not either of the two above, then you must return and 
	print an error.

	- Remember that you must write to "Output.txt" in this function. (Use the FileIOManager)
-------------------------------------------------------------------------------------------------*/
void GSM_UpdatePtrs()
{
	
}

/*!------------------------------------------------------------------------------------------------
	\brief Implements the gameloop seen in class. 
	\details
		Remember that the game loop is implemented using two loops: The inner loop calls Update and 
		Render as long as we're not changing game state (that is, the current game state and the next are
		still the same value). The outer loop handles game state changing. This implies game state
		loading/unloading, initialization and freeing, as well as handling the GS_QUIT and GS_RESTART 
		special game states. 
------------------------------------------------------------------------------------------------*/
void GSM_GameLoop()
{
	// TODO
}
