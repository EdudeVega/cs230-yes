#include <AEEngine.h>


AEGfxTriList * MakeQuad(u32 color)
{
	
	// Notify graphics that we are about to build a triangle list
	AEGfxTriStart();

	AEGfxTriAdd(
		-.50f, .5f, color, 0.0f, 0.0f, 
		-.50f, -.50f, color, 0.0f, 1.0f, 
		.50f, -.50f, color, 1.0f, 1.0f);
	
	AEGfxTriAdd(
		-.50f, .5f, color, 0.0f, 0.0f, 
		.50f, -.50f, color, 1.0f, 1.0f,
		.50f, .50f, color, 1.0f, 0.0f );

	// End the triangle list creation process and get the final list
	return AEGfxTriEnd();
}
AEGfxTriList * MakeCircle(u32 precision, u32 color)
{
	// the algorithm works as follows:
	// using the unit circle create a triangle fan (i.e, the first vertex is always
	// the same) with the first vertex as the center and the other vertices representing
	// the next two vertices with coordinates cos(angle), sin(angle) and cos(angle + prec),
	// sin(angle + prec)

	f32 PI2 = 2*(f32)PI;
	f32 angle_inc = PI2 / (f32)precision;
	f32 angle = 0.0f;
	AEVector2 v0 = {0.0f, 0.0f};
	AEVector2 v1, v2;

	AEGfxTriStart();

	for(; angle < PI2; angle+=angle_inc)
	{
		v1.x = AECos(angle); 
		v1.y = AESin(angle);
		v2.x = AECos(angle + angle_inc); 
		v2.y = AESin(angle + angle_inc);

		// add the triangle
		AEGfxTriAdd(
			v0.x, v0.y, color, 0.0f, 0.0f, 
			v1.x, v1.y, color, 0.0f, 0.0f,
			v2.x, v2.y, color, 0.0f, 0.0f);
	}
	
	return AEGfxTriEnd();
}