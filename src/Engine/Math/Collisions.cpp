// ----------------------------------------------------------------------------
//	Copyright (C)DigiPen Institute of Technology.
//	Reproduction or disclosure of this file or its contents without the prior
//	written consent of DigiPen Institute of Technology is prohibited.
//	
//	File Name:		Collisions.h
//	Purpose:		Declares static containment and overlap tests between 
//					different primitives.
//	Project:		cs230_tkomair_2_2
//	Author:			Thomas Komair (tkomair@digipen.edu)
// ----------------------------------------------------------------------------
#include "Collisions.h"
#include "Vector2.h"
#include "Matrix33.h"
#include "Transform2D.h"
// ----------------------------------------------------------------------------


//! ---------------------------------------------------------------------------
// \fn		StaticPointToStaticCircle
// \brief	This function determines if a static point is inside a static circle
// 			- P:		The point to test for circle containment
// 			- Center:	Center of the circle
// 			- Radius:	Radius of the circle
// 
//  \return	true if the point is contained in the circle, false otherwise.
// ---------------------------------------------------------------------------
bool StaticPointToStaticCircle(Vector2 * P, Vector2 * Center, float Radius) {

	//Calculate if the distance between the center and the point and compare it with the radius
	return (abs((Center->x - P->x) * (Center->x - P->x)) + abs((Center->y - P->y) * (Center->y - P->y)) <= (Radius * Radius));
}

//! ---------------------------------------------------------------------------
//	\fn		StaticPointToStaticRect
//	\brief	This function checks if the point Pos is colliding with the rectangle
//			whose center is Rect, width is "Width" and height is Height
//
//  \return	true if the point is contained in the rectangle, false otherwise.
// ---------------------------------------------------------------------------
bool StaticPointToStaticRect(Vector2* Pos, Vector2* Rect, float Width, float Height) {

	//Calculate all the corners of the AABB
	float l = Rect->x -	(Width / 2);
	float r = Rect->x +	( Width / 2 );
	float b = Rect->y -	( Height / 2);
	float t = Rect->y +	( Height / 2);

	return (l <= Pos->x && Pos->x <= r && b <= Pos->y && Pos->y <= t);
}
//! ---------------------------------------------------------------------------
//	\fn		StaticPointToOrientedRect
//	\brief	This function checks if the point Pos is colliding with an oriented rectangle
//			whose center is Rect, width is "Width", height is Height and rotation "AngleRad"
//
//  \return	true if the point is contained in the rectangle, false otherwise.
// ---------------------------------------------------------------------------
bool StaticPointToOrientedRect(Vector2* Pos, Vector2* Rect, float Width, float Height, float AngleRad) {
	Vector2 vec(Pos->x - Rect->x , Pos->y - Rect->y);
	Vector2 u = vec.Project(Vector2(cos(AngleRad), sin(AngleRad)));
	Vector2 v = vec.Project(Vector2(-sin(AngleRad), cos(AngleRad)));
	return (u.LengthSq() <= ((Width / 2) * (Width / 2)) && v.LengthSq() <= ((Height / 2) * (Height / 2)));

		
}

//! ---------------------------------------------------------------------------
//	\fn		StaticPointToStaticCircle
//	\brief	This function checks for collision between 2 circles.
//			Circle0: Center is Center0, radius is "Radius0"
//			Circle1: Center is Center1, radius is "Radius1"
//
//  \return	true if both circles overlap, false otherwise.
// ---------------------------------------------------------------------------
bool StaticCircleToStaticCircle(Vector2* Center0, float Radius0, Vector2* Center1, float Radius1) {
	
	//Calculate if the distance between the center and the other and compare it with the radius
	return (abs((Center0->x - Center1->x) * (Center0->x - Center1->x)) + abs((Center0->y - Center1->y) * (Center0->y - Center1->y)) <= ((Radius1 + Radius0)*(Radius1 + Radius0)));
}

//! ---------------------------------------------------------------------------
// \fn		StaticRectToStaticRect
// \brief	This functions checks if 2 rectangles are colliding
//			Rectangle0: Center is pRect0, width is "Width0" and height is "Height0"
//			Rectangle1: Center is pRect1, width is "Width1" and height is "Height1"
//
//  \return	true if both rectangles overlap, false otherwise.
// ---------------------------------------------------------------------------
bool StaticRectToStaticRect(Vector2* Rect0, float Width0, float Height0, Vector2* Rect1, float Width1, float Height1) {
	float w = Width0 + Width1;
	float h = Height0 + Height1;
	return StaticPointToStaticRect(Rect1, Rect0, w, h);
}

//! ---------------------------------------------------------------------------
// \fn		StaticRectToStaticCirlce
// \brief	This function checks if a circle is colliding with a rectangle
//			Rectangle: Defined at center "Rect" of width and height "Width", "Height", respectively
//			Center: Defined at center "Center", and of radius "Radius"
//
//  \return	true if both shapes overlap, false otherwise.
// ---------------------------------------------------------------------------
bool StaticRectToStaticCirlce(Vector2* Rect, float Width, float Height, Vector2* Center, float Radius) {
	//Calculate all the corners of the AABB
	float l = Rect->x - (Width / 2);
	float r = Rect->x + (Width / 2);
	float b = Rect->y - (Height / 2);
	float t = Rect->y + (Height / 2);

	//CLAMPED circle
	Vector2 clamped(max(l, min(r,Center->x) ) , max(b, min(t, Center->y)));

	//check with the clamped circle
	return StaticPointToStaticCircle(&clamped, Center, Radius);
}

//! ---------------------------------------------------------------------------
// \fn		OrientedRectToStaticCirlce
// \brief	This function checks if a circle is colliding with an oriented rectangle
//			Rectangle: Defined at center "Rect" of width and height "Width", "Height", 
//			and rotation "AngelRad" respectively
//			Center: Defined at center "Center", and of radius "Radius"
//
//  \return	true if both shapes overlap, false otherwise.
// ---------------------------------------------------------------------------
bool OrientedRectToStaticCirlce(Vector2* Rect, float Width, float Height, float AngleRad, Vector2* Center, float Radius) {
	
	Transform2D trans(*Rect, Vector2(1, 1), AngleRad);
	Vector2 NewCir = trans.GetInvMatrix() * *Center;
	return StaticRectToStaticCirlce(&Vector2(0,0), Width, Height, &NewCir, Radius);
	return false;
}

