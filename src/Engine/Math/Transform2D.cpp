/*!
*	\file		Transfrom2D.cpp
*	\brief		Transform2D oeprator and functions
*	\details	Operations for the Transformations clas
*	\author		Eduardo de Vega - eduardo.devega@digipen.edu
*	\date		10/02/2020
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

#include "Transform2D.h"
// ---------------------------------------------------------------------------


//! ---------------------------------------------------------------------------
// \fn		Transform2D
// \brief	Default constructor. Sets the transform to identity
// ---------------------------------------------------------------------------
Transform2D::Transform2D() : mScale(1,1),mPosition(0,0),mOrientation(0){}//set the values


//! ---------------------------------------------------------------------------
// \fn		Transform2D
// \brief	Custom constructor. Sets the transform with the provided values.
// ---------------------------------------------------------------------------
Transform2D::Transform2D(const Vector2 & pos, const Vector2 & scale, const float & rot):mPosition(pos),mScale(scale),mOrientation(rot){}//set the values


//! ---------------------------------------------------------------------------
// \fn		Transform2D
// \brief	Custom constructor. Sets the transform with the provided values.
// ---------------------------------------------------------------------------
Transform2D::Transform2D(float tx, float ty, float sx, float sy, float rot) : mScale(sx, sy), mPosition(tx, ty), mOrientation(rot)//set the values
{}


//! ---------------------------------------------------------------------------
// \fn		SetIdentity
// \brief	Sets this transform as the identity.
// ---------------------------------------------------------------------------
void Transform2D::SetIdentity(){
	//set the scale as 1,1
	mScale = Vector2(1, 1);
	mPosition = Vector2(0, 0);
	mOrientation = 0;
}



//! ---------------------------------------------------------------------------
// \fn		GetMatrix
// \brief	Returns the local to world matrix such that M = T*R*S
// ---------------------------------------------------------------------------
Matrix33 Transform2D::GetMatrix()const{
	//Calculate the matrix
	Matrix33 trans;
	Matrix33 sca;
	Matrix33 rot;
	Matrix33 mat;
	trans = trans.Translate(mPosition.x, mPosition.y);
	sca = sca.Scale(mScale.x, mScale.y);
	rot = rot.RotRad(mOrientation);
	//and return it
	mat = trans * rot * sca;
	return mat;
}



//! ---------------------------------------------------------------------------
// \fn		GetInvMatrix
// \brief	Returns the world to local matrix such that Minv = Sinv*Rinv*Tinv.
//			IMPORTANT:
//			You can't use AEMtx33Inverse for this function, you must compute
//			each inverse matrix separately and concatenate them in the correct
//			order. 
// ---------------------------------------------------------------------------
Matrix33 Transform2D::GetInvMatrix()const{
	//Calculate the inv matrix
	Matrix33 trans;
	Matrix33 sca;
	Matrix33 rot;
	Matrix33 mat;
	//sanity check
	if (!mScale.x || !mScale.y)
		sca = sca.Scale(mScale.x, mScale.y);
	else
		sca = sca.Scale(1/mScale.x,1/ mScale.y);
	trans = trans.Translate(-mPosition.x, -mPosition.y);
	rot = rot.RotRad(-mOrientation);
	//return it
	mat = sca  * rot * trans;
	return mat;
}



//! ---------------------------------------------------------------------------
// \fn		Concat
// \brief	Transform concatenation as seen in class, such that 
//			result = this * rhs.
//			IMPORTANT: YOU CAN'T USE MATRICES IN THIS FUNCTION
// ---------------------------------------------------------------------------
Transform2D Transform2D::Concat(const Transform2D & rhs)const{
	//Using the operator
	return (*this * rhs);
}
Transform2D Transform2D::InverseConcat(const Transform2D& rhs)const {
	Matrix33 trans = (*this).GetInvMatrix();
	Transform2D mat;
	//Set the position
	mat.mPosition = trans * rhs.mPosition;

	//the scale
	mat.mScale.x = rhs.mScale.x / mScale.x ;
	mat.mScale.y = rhs.mScale.y / mScale.y ;
	//and rotation
	mat.mOrientation = mOrientation - rhs.mOrientation;

	return mat;
	
}

//! ---------------------------------------------------------------------------
// \fn		operator *
// \brief	Same transform concatenation as above. In operator form.
// ---------------------------------------------------------------------------
Transform2D Transform2D::operator *(const Transform2D & rhs)const{
	Transform2D mat;
	//Set the position
	mat.mPosition.x = mPosition.x + cos(mOrientation) *  rhs.mPosition.x * mScale.x - sin(mOrientation)* rhs.mPosition.y * mScale.y;
	mat.mPosition.y = mPosition.y + sin(mOrientation) *  rhs.mPosition.x * mScale.x + cos(mOrientation)* rhs.mPosition.y * mScale.y;
	//the scale
	mat.mScale.x = mScale.x * rhs.mScale.x;
	mat.mScale.y = mScale.y * rhs.mScale.y;
	//and rotation
	mat.mOrientation = mOrientation + rhs.mOrientation;

	return mat;
}


//! ---------------------------------------------------------------------------
// \fn		operator *=
// \brief	Concatenates this transform with rhs and stores the result in this 
//			transform, such that *this = *this *rhs;
// ---------------------------------------------------------------------------
Transform2D & Transform2D::operator *=(const Transform2D &rhs){
	//multiply it and return it
	*this = (*this * rhs);
	return *this;
}



//! ---------------------------------------------------------------------------
// \fn		MultPoint
// \brief	Vector multiplication. Returns the resulting vector of multiplying
//			rhs by the local to world matrix
// ---------------------------------------------------------------------------
Vector2 Transform2D::MultPoint(const Vector2 & rhs)const{
	//Using operator
	//Using operator
	Matrix33 mat = GetMatrix();
	Vector2 res(mat.m[0][0] * rhs.x + mat.m[0][1] * rhs.y + mat.m[0][2],
		mat.m[1][0] * rhs.x + mat.m[1][1] * rhs.y + mat.m[1][2]);


	return res;
}


//! ---------------------------------------------------------------------------
// \fn		operator *
// \brief	Vector multiplication. Returns the resulting vector of multiplying
//			rhs by the local to world matrix
// ---------------------------------------------------------------------------
Vector2 Transform2D::operator *(const Vector2 & rhs)const{
	//Using operator
	Matrix33 mat = GetMatrix();
	Vector2 res(mat.m[0][0] * rhs.x + mat.m[0][1] * rhs.y + mat.m[0][2],
		mat.m[1][0] * rhs.x + mat.m[1][1] * rhs.y + mat.m[1][2]);


	return res;
}


//! ---------------------------------------------------------------------------
// \fn		MultPointArray
// \brief	transform each vector in the given array 'vecArray' with 'size' 
//			elements. Don't forget sanity checks.
// ---------------------------------------------------------------------------
void Transform2D::MultPointArray(Vector2 * vecArray, int size)const{
	//Check if not empty
	if (!size)
		return;
	for (int i = 0; i < size; i++)
		//Using operator
		vecArray[i] = (GetMatrix() * vecArray[i]);
}
