/*!
*	\file		Matrix33.cpp
*	\brief		3x3 Matrix oeprator and functions
*	\details	Opeerations for the matrix clas
*	\author		Eduardo de Vega - eduardo.devega@digipen.edu
*	\date		10/02/2020
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/
#include "MyMath.h"
#include <string.h>	// memcpy


//! ---------------------------------------------------------------------------
// \fn		Matrix33()
// \brief	Default Constructor - Sets the matrix to identity.
// ---------------------------------------------------------------------------
Matrix33::Matrix33(){

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			//Set everything to zero
			m[i][j] = 0;
			if (i == j)
				//but the identity
				m[i][j] = 1;
		}
	}
}


//! ---------------------------------------------------------------------------
// \fn		Matrix33()
// \brief	Custom Constructor - Sets the elements as the one specified.
// ---------------------------------------------------------------------------
Matrix33::Matrix33(float a11, float a12, float a13,
	float a21, float a22, float a23,
	float a31, float a32, float a33) {
	//set all the numbers
	m[0][0] = a11;
	m[0][1] = a12;
	m[0][2] = a13;
	m[1][0] = a21;
	m[1][1] = a22;
	m[1][2] = a23;
	m[2][0] = a31;
	m[2][1] = a32;
	m[2][2] = a33;
}


//! ---------------------------------------------------------------------------
// \fn		SetIdentity
// \brief	Set this matrix to identity.
// ---------------------------------------------------------------------------
void Matrix33::SetIdentity(){
	//Same as default constructor
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			m[i][j] = 0;
			if (i == j)
				m[i][j] = 1;
		}
	}
}


//! ---------------------------------------------------------------------------
// \fn		Transpose
// \brief	return the transpose version of the matrix.
// ---------------------------------------------------------------------------
Matrix33 Matrix33::Transpose()const{
	Matrix33 mat;
	//set the rows as columns and oposite
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			mat.m[j][i] = m[i][j];
			
		}
	}
	return mat;
}


//! ---------------------------------------------------------------------------
// \fn		TransposeThis
// \brief	Sets this matrix as its transpose. 
// ---------------------------------------------------------------------------
Matrix33& Matrix33::TransposeThis(){
	//sets itself to the transpose
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			m[j][i] = m[i][j];

		}
	}

	return *this;
}


//! ---------------------------------------------------------------------------
// \fn		Concat
// \brief	Concatenate this matrix with 'rhs' such that result = this * rhs.
// ---------------------------------------------------------------------------
Matrix33 Matrix33::Concat(const Matrix33 & rhs)const{
	//multiply them
	Matrix33 mat = (*this * rhs);
	return mat;
}


//! ---------------------------------------------------------------------------
// \fn		operator *
// \brief	Same as above but in operator form. 
// ---------------------------------------------------------------------------
Matrix33 Matrix33::operator *(const Matrix33 & rhs)const
{
	Matrix33 mat;
	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){

			mat.m[i][j] = 0;
			for (int k = 0; k < 3; k++){
				mat.m[i][j] += m[i][k] * rhs.m[k][j];
			}
			
		}
	}
	return mat;
	
}


//! ---------------------------------------------------------------------------
// \fn		operator *=
// \brief	Concatenates this with 'rhs' as above and stores the result in 
//			this matrix. Returns this matrix.
// ---------------------------------------------------------------------------
Matrix33& Matrix33::operator *=(const Matrix33 &rhs)
{
	Matrix33 mat;
	//multiplies it and return it
	mat = *this * rhs;
	*this = mat;
	return *this;
}


//! ---------------------------------------------------------------------------
// \fn		MultPoint
// \brief	Matrix to 2D Point multiplication such that result = this * rhs
// ---------------------------------------------------------------------------
Vector2 Matrix33::MultPoint(const Vector2 & vec)const{

	//Multiping and applying the transformation
	Vector2 res(m[0][0] * vec.x + m[0][1] * vec.y,
		m[1][0] * vec.x + m[1][1] * vec.y);


	return res;
}


//! ---------------------------------------------------------------------------
// \fn		operator *
// \brief	Matrix to 2D Vector multiplication such that result = this * rhs
//			same as above but in operator form.
// ---------------------------------------------------------------------------
Vector2 Matrix33::operator*(const Vector2 & vec)const{
	//Multiping and applying the transformation
	Vector2 res(m[0][0] * vec.x + m[0][1] * vec.y,
	m[1][0] * vec.x + m[1][1] * vec.y);
	res.x += m[0][2];
	res.y += m[1][2];

	return res;
}


//! ---------------------------------------------------------------------------
// \fn		MultVecDir 
// \brief	MultVec - Multiplies the vector by the matrix such that only the 
//			upper-left 2x2 matrix is considered (we assume we are multiplying a 
//			2D vector and not a point.
// ---------------------------------------------------------------------------
Vector2 Matrix33::MultVec(const Vector2 &vec)const{
	//Multiping only
	Vector2 res(m[0][0] * vec.x + m[0][1] * vec.y,
		m[1][0] * vec.x + m[1][1] * vec.y);
	return res;
}

//! ---------------------------------------------------------------------------
// Static Interface
//! ---------------------------------------------------------------------------

// @PROVIDED
//! ---------------------------------------------------------------------------
// \fn		Matrix33::Identity
// \brief	Returns a matrix as the identity.
// ---------------------------------------------------------------------------
Matrix33 Matrix33::Identity()
{
	// default constructor constructs an identidy matrix
	return Matrix33();
}


//! ---------------------------------------------------------------------------
// \fn		Matrix33::Translate
// \brief	Returns a translation matrix by a vector (x,y).
// ---------------------------------------------------------------------------
Matrix33 Matrix33::Translate(float x, float y){
	Matrix33 mat = Identity();
	//Set the translate 
	mat.m[0][2] = x;
	mat.m[1][2] = y;
	return mat;
}


//! ---------------------------------------------------------------------------
// \fn		Matrix33::Scale
// \brief	Returns a scaling matrix by a factor (sx, sy).
// ---------------------------------------------------------------------------
Matrix33 Matrix33::Scale(float sx, float sy){

	Matrix33 mat = Identity();
	//Set the scale values
	mat.m[0][0] = sx;
	mat.m[1][1] = sy;

	return mat;
}


//! ---------------------------------------------------------------------------
// \fn		Matrix33::RotDeg
// \brief	Returns a CCW rotation matrix by an angle specified in degrees. 
// ---------------------------------------------------------------------------
Matrix33 Matrix33::RotDeg(float angle_deg){
	Matrix33 mat = Identity();
	//set it to rad
	float angle_rad = angle_deg * (PI / 180);
	//the set it into the matrix
	mat.m[0][0] = cos(angle_rad);
	mat.m[0][1] = -sin(angle_rad);
	mat.m[1][0] = sin(angle_rad);
	mat.m[1][1] = cos(angle_rad);

	return mat;
}


//! ---------------------------------------------------------------------------
// \fn		Matrix33::RotRad
// \brief	Returns a CCW rotation matrix by an angle specified in radians.
// ---------------------------------------------------------------------------
Matrix33 Matrix33::RotRad(float angle_rad){

	Matrix33 mat = Identity();
	//Put the data in the matrix
	mat.m[0][0] = cos(angle_rad);
	mat.m[0][1] = -sin(angle_rad);
	mat.m[1][0] = sin(angle_rad);
	mat.m[1][1] = cos(angle_rad);

	return mat;
}