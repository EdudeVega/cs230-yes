/*!
*	\file		Vector.cpp
*	\brief		Vector2 oeprator and functions
*	\details	Opeerations forthe vector clas
*	\author		Eduardo de Vega - eduardo.devega@digipen.edu
*	\date		10/02/2020
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

#include <ctime>	//time
#include "MyMath.h"
#include <random>

//! ---------------------------------------------------------------------------
// \fn		Default Constructor.
// \brief	Sets the vector to (0,0).
// ---------------------------------------------------------------------------
Vector2::Vector2() : x(0),y(0){}//set it to 0,0

//! ---------------------------------------------------------------------------
// \fn		Custom Constructor
// \brief	Sets to the specified values.
// ---------------------------------------------------------------------------
Vector2::Vector2(float xx, float yy) : x(xx), y(yy) {}//set it to xx and yy		

//! ---------------------------------------------------------------------------
// \fn		operator+
// \brief	Vector addition such that result = *this + rhs
// ---------------------------------------------------------------------------
Vector2	Vector2::operator+(const Vector2& rhs) const{
	//Create a vector with the sum of the two
	Vector2 result = { x + rhs.x , y + rhs.y };
	//return it
	return result;
}

 //! ---------------------------------------------------------------------------
// \fn		operator+=
// \brief	Vector addition such that *this = *this + rhs
// ---------------------------------------------------------------------------
Vector2& Vector2::operator+=(const Vector2& rhs){
	//return the multiplication
	return *this = *this + rhs;
}


//! ---------------------------------------------------------------------------
// \fn		operator-
// \brief	Vector subtraction such that result = *this - rhs
// ---------------------------------------------------------------------------
Vector2		Vector2::operator-	(const Vector2& rhs) const{
	//return the vector result of the substarction
	Vector2 result = { x - rhs.x , y - rhs.y };
	return result;
}


//! ---------------------------------------------------------------------------
// \fn		operator-=
// \brief	Vector subtraction such that *this = *this - rhs
// ---------------------------------------------------------------------------
Vector2&		Vector2::operator-=	(const Vector2& rhs){
	//return the substacted *this
	return *this = *this - rhs;
}


//! ---------------------------------------------------------------------------
// \fn		operator*
// \brief	Scalar multiplication such that result = *this * s
// ---------------------------------------------------------------------------
Vector2		Vector2::operator*	(float s) const{
	//Create a vector with the multiplication and return it
	Vector2 result = { x * s , y * s };
	return result;
}


//! ---------------------------------------------------------------------------
// \fn		operator/
// \brief	Scalar division such that result = *this / s
// ---------------------------------------------------------------------------
Vector2		Vector2::operator/	(float s) const{
	//Create a vector with the division and return it
	Vector2 result = { x / s , y / s };
	return result;
}


//! ---------------------------------------------------------------------------
// \fn		operator*=
// \brief	Scalar multiplication such that *this = *this * s
// ---------------------------------------------------------------------------
Vector2&		Vector2::operator*=	(float s){
	//return the multiplied vector
	return *this = *this * s;
	
}


//! ---------------------------------------------------------------------------
// \fn		operator/=	
// \brief	Scalar division such that *this = *this / s
// ---------------------------------------------------------------------------
Vector2&		Vector2::operator/=	(float s){
	//return the divided vector
	return *this = *this / s;

}


//! ---------------------------------------------------------------------------
// \fn		operator-
// \brief	unitary negation - returns a vector equal to this vector negated
// ---------------------------------------------------------------------------
Vector2		Vector2::operator-() const{
	//return negated vector
	return Vector2(-this->x, -this->y);
}


//! ---------------------------------------------------------------------------
// \fn		Length
// \brief	Returns the length of this vector.
// ---------------------------------------------------------------------------
float			Vector2::Length() const{
	//Calculate the lenght
	return sqrt(x*x + y * y);
}


//! ---------------------------------------------------------------------------
// \fn		LengthSq
// \brief	Returns the squared length of this vector.
// ---------------------------------------------------------------------------
float			Vector2::LengthSq() const{
	//Calulate the length without the sqrt
	return x * x + y * y;
}


//! ---------------------------------------------------------------------------
// \fn		Distance
// \brief	Returns the distance from this vector to 'rhs'
// ---------------------------------------------------------------------------
float			Vector2::Distance(const Vector2 &rhs){
	//Formula for distance between two vector
	return sqrt( (x - rhs.x)*(x - rhs.x) + (y - rhs.y)*(y - rhs.y));
}


//! ---------------------------------------------------------------------------
// \fn		DistanceSq
// \brief	Returns the squared distance from this vector to 'rhs'
// ---------------------------------------------------------------------------
float			Vector2::DistanceSq(const Vector2 &rhs){
	//The distance without using sqrt
	return (x - rhs.x)*(x - rhs.x) + (y - rhs.y)*(y - rhs.y);
}

//! ---------------------------------------------------------------------------
// \fn		Normalize
// \brief	Returns the normalized version of this vector
// ---------------------------------------------------------------------------
Vector2		Vector2::Normalize() const{
	//the vector divided by its lenght
	return *this / Length();
}


//! ---------------------------------------------------------------------------
// \fn		NormalizeThis
// \brief	Computes the normalized version of this vector and set this vector
//			to it. 
// ---------------------------------------------------------------------------
Vector2 & Vector2::NormalizeThis(){
	if(Length())
	//return the normalized vector
	return (*this /= Length());


}

//! ---------------------------------------------------------------------------
// \fn		Dot
// \brief	Dot product
// ---------------------------------------------------------------------------
float Vector2::Dot(const Vector2& rhs) const{
	//using the operators
	return *this * rhs;
}


//! ---------------------------------------------------------------------------
// \fn		operator*
// \brief	Dot product (operator form)
// ---------------------------------------------------------------------------
float Vector2::operator* (const Vector2& rhs) const{
	//calculating the dot product and returning it
	return (x * rhs.x) + (y * rhs.y);
}



//! ---------------------------------------------------------------------------
// \fn		Perp
// \brief	Returns a vector equal to the rotation of this vector by
//			-90 degrees. DO NOT USE A ROTATION MATRIX.
// ---------------------------------------------------------------------------
Vector2		Vector2::Perp(){
	//Return the perpendicular vector
	Vector2 perped = { -y , x };
	return perped;
}


//! ---------------------------------------------------------------------------
// \fn		Project 
// \brief	Computes the projection of this vector onto rhs.
// ---------------------------------------------------------------------------
Vector2		Vector2::Project(const Vector2& rhs){
	//Calulcate the projection with the formula 
	Vector2 projected = rhs * ((*this * rhs) / (rhs * rhs)) ;
	return projected;
}


//! ---------------------------------------------------------------------------
// \fn		ProjectPerp
// \brief	Computes the perpendicular projection of this vector onto rhs.
// ---------------------------------------------------------------------------
Vector2		Vector2::ProjectPerp(const Vector2& rhs){
	//Calulcate the projection with the formula and substract *this
	Vector2 projected =  *this - ( rhs * ((*this * rhs) / (rhs * rhs))) ;
	return projected ;
}


//! ---------------------------------------------------------------------------
// \fn		CrossMag
// \brief	Returns the cross product magnitude.
// ---------------------------------------------------------------------------
float			Vector2::CrossMag(const Vector2& rhs){
	//Calulcate the cross magnitude
	return x * rhs.y - y * rhs.x;
}


//! ---------------------------------------------------------------------------
// \fn		GetAngle
// \brief	Returns and angle (in radians) that represents the CCW positive
//			angle of this vector.
// ---------------------------------------------------------------------------
float			Vector2::GetAngle(){

	//get the angle between both
	return atan2( y,x );
}


//! ---------------------------------------------------------------------------
// \fn		FromAngle
// \brief	Sets this vector as the result of getting a unit vector 
//			from the specified angle in radians.
// ---------------------------------------------------------------------------
void		Vector2::FromAngle(float rad_angle){
	//Set the angles
	x = cos(rad_angle);
	y = sin(rad_angle);

}

//! ---------------------------------------------------------------------------
// Static interface
//! ---------------------------------------------------------------------------


//! ---------------------------------------------------------------------------
// \fn		Random
// \brief	Returns a random vector such that min_x < x < max_x  and 
//			min_y < y < max_y.
//	\note	This function should use rand(). Remember that rand() returns an 
//			an integer value between 0 and RAND_MAX
// ---------------------------------------------------------------------------
Vector2 Vector2::Random(float min_x, float max_x, float min_y, float max_y){
	//set the seed for the rands
	srand(time(NULL));
	//calculate it betwwen the range
	float x = min_x + (rand() % (int)max_x);
	float y = min_y + (rand() % (int)max_y);
	//return it
	return Vector2(x, y);
}


//! ---------------------------------------------------------------------------
// \fn		Random01
// \brief	Returns a random vector such that x and y are both in the range
//			[0,1]
//	\note	X and Y should not be the same (i.e. don't use the same random)
// ---------------------------------------------------------------------------
Vector2 Vector2::Random01() {
	//set the seed for the rands
	srand(time(NULL));
	//Divide by the Rand max to get it between 0 and 1
	float x = rand() / (float)(RAND_MAX);
	float y = rand() / (float)(RAND_MAX);
	//return it
	return Vector2(x, y);
}


//! ---------------------------------------------------------------------------
// \fn		Lerp 
// \brief	Returns the result vector of linearly interpolating from 
//			start to end using a normalized parameter tn (i.e. tn is in the 
//			range [0,1].
// ---------------------------------------------------------------------------
Vector2 Vector2::Lerp(Vector2 start, Vector2 end, float tn){
	Vector2 vec;
	//Formula of linear interpolation on x and y
	vec.x = (1 - tn) * start.x + tn * end.x ;
	vec.y = (1 - tn) * start.y + tn * end.y ;
	//return its
	return vec;
}
