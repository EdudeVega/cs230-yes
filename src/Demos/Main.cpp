/*!
*	\file		Main.cpp
*	\brief		Entry point of the program
*	\author		Thomas Komair - tkomair@digipen.edu
*	\date		12/02/2018
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/


#include <AEEngine.h>

// Put your includes here
#include "..\Engine\Engine.h"

// Include Levels Here
#include "GameStateList.h"
#include "TankLevel.h"
#include "TransformConcatDemo.h"
#include "TestStaticCollisions.h"

#include <iostream>
using std::cout;
using std::endl;

/*------------------------------------------------------------------------------------------------
 Function: main
 Purpose: Entry point of the program
-------------------------------------------------------------------------------------------------*/
int main()
{
	// Initialize the alpha engine
	AESysInit("CS230 - Tanks!");

	// Adding Tank Level
	AEGameStateMgrAdd(
		GS_TEST_STATIC_COLLISIONS,
		TestStaticCollisions_Load,
		TestStaticCollisions_Init,
		TestStaticCollisions_Update,
		TestStaticCollisions_Render,
		TestStaticCollisions_Free,
		TestStaticCollisions_Unload);

	// Adding Tank Level
	AEGameStateMgrAdd(
		GS_TANK,
		TankLevel_Load,
		TankLevel_Init,
		TankLevel_Update,
		TankLevel_Render,
		TankLevel_Free,
		TankLevel_Unload);

	// Adding Tank Level
	AEGameStateMgrAdd(
		GS_TRANSFORM_DEMO,
		TransformConcatDemo_Load,
		TransformConcatDemo_Init,
		TransformConcatDemo_Update,
		TransformConcatDemo_Draw,
		TransformConcatDemo_Free,
		TransformConcatDemo_Unload);

	// Initialize game state manager
	AEGameStateMgrInit(GS_TRANSFORM_DEMO);
	
	// Run Game loop
	AESysGameLoop();

	// Terminate Alpha Engine
	AESysExit();
	

	return 0;
}
