// ---------------------------------------------------------------------------
// Project Name		:	Alpha Engine
// File Name		:	Level.h
// Author			:	Thomas Komair
// Creation Date	:	2013/04/26
// Purpose			:	Sample header for a demo level
// History			:
// ---------------------------------------------------------------------------

#ifndef LEVEL_1_H_	// Insure we only include this file once.
#define LEVEL_1_H_

#define GS_LEVEL_1 1
void TransformConcatDemo_Load();
void TransformConcatDemo_Init(); 
void TransformConcatDemo_Update(); 
void TransformConcatDemo_Draw();
void TransformConcatDemo_Free();
void TransformConcatDemo_Unload();

#endif