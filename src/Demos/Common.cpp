#include <AEEngine.h>
#include "..\Engine\Engine.h"
#include "GameStateList.h"
#include "Common.h"
#include <cstdlib>

Model* MakeQuad()
{
	Model* model = new Model;
	model->mVertexCount = 8;
	model->mVertexArray = new Vector2[model->mVertexCount];

	// temp
	f32 h = 0.5f;

	// left side
	model->mVertexArray[0] = { -h, h };
	model->mVertexArray[1] = { -h, -h };

	// bottom side
	model->mVertexArray[2] = { -h, -h };
	model->mVertexArray[3] = { h, -h };

	// right side
	model->mVertexArray[4] = { h, -h };
	model->mVertexArray[5] = { h, h };

	// top side
	model->mVertexArray[6] = { -h, h };
	model->mVertexArray[7] = { h, h };

	// return newly created model
	return model;
}
Model * MakeCircle()
{
	// create new model
	Model* model = new Model;

	// allocate memory for the vertices
	const u32 precision = 20;
	model->mVertexCount = precision * 2;
	model->mVertexArray = new Vector2[model->mVertexCount];


	// generate circle model
	float angle = 0.0f;
	float angle_offset = TWO_PI / (float)precision;
	float hrad = .5f;
	int i = 0;
	while (angle < TWO_PI)
	{
		float p0x = AECos(angle) * hrad; float p0y = AESin(angle) * hrad;
		angle += angle_offset;
		float p1x = AECos(angle) * hrad; float p1y = AESin(angle) * hrad;

		model->mVertexArray[i].x = p0x;	
		model->mVertexArray[i++].y = p0y;
		model->mVertexArray[i].x = p1x;
		model->mVertexArray[i++].y = p1y;

	}

	// return newly created model
	return model;
}
void DrawModel(Model* model, const Transform2D& transform, unsigned color)
{
	AEMtx33 world = transform.GetMatrix();
	AEGfxSetTransform(&world);
	for (u32 i = 0; i < model->mVertexCount; ++i)
	{
		// use pointers to reference the vector
		Vector2 * p0, *p1;

		if (i == model->mVertexCount - 1)
		{
			p0 = model->mVertexArray + i; // last vertex
			p1 = model->mVertexArray;		// first vertex
		}
		else
		{
			p0 = model->mVertexArray + i;		// current vertex
			p1 = model->mVertexArray + i + 1; // next vertex
		}
		AEGfxLine(p0->x, p0->y, 0.0f, color, p1->x, p1->y, 0.0f, color);
	}
	AEGfxFlush();
}
void FreeModel(Model* model)
{
	// sanity check
	if (!model)return;

	// free vertex array
	if(model->mVertexArray)
		delete [] model->mVertexArray;

	// free model
	delete model;
}

// Game Object Manager
int					gObjectCount = 0;
GameObject			gvAllObjects[MAX_OBJECT_NUM];


GameObject* NewGameObject(Vector2 pos, Vector2 sca, f32 rot, u32 shapeType, u32 color, Model* model)
{
	// Make sure we haven't reached the end of the game object array
	if (gObjectCount >= MAX_OBJECT_NUM)
		return NULL;

	// Get a new game object
	GameObject* newObj = gvAllObjects + gObjectCount;

	// Set initial data
	newObj->mTransform.mPosition = pos;
	newObj->mTransform.mScale = sca;
	newObj->mTransform.mOrientation = rot;
	newObj->mpModel = model;
	newObj->mCollisionShape = shapeType;
	newObj->mColor = color;
	newObj->mAlive = true;

	// increment the object count
	gObjectCount++;

	// Done! object created successfully.
	return newObj;
}

void DrawAllGameObjects()
{
	for (int i = 0; i < gObjectCount; ++i)
		DrawGameObject(gvAllObjects + i);
}

void DestroyObject(GameObject* obj)
{
	obj->mAlive = false;
}

void ClearAllObjects()
{
	gObjectCount = 0;
}

void DrawGameObject(GameObject* pObj)
{
	// Sanity Check
	if (NULL == pObj || pObj->mAlive == false)
		return;

	// draw model with object data
	DrawModel(pObj->mpModel, pObj->mTransform, pObj->mColor);
}


void GameFlow()
{
	// Extra logic
	// Quit
	if (AEInputKeyTriggered('Q') && AEInputKeyPressed(VK_CONTROL))
		gAEGameStateNext = GS_QUIT;
	// Restart
	if (AEInputKeyTriggered('R') && AEInputKeyPressed(VK_CONTROL))
		gAEGameStateNext = GS_RESTART;

	// TEST STATIC COLLISION GAME STATE
	if (AEInputKeyTriggered('1'))
		gAEGameStateNext = GS_TEST_STATIC_COLLISIONS;
	if (AEInputKeyTriggered('2'))
		gAEGameStateNext = GS_TANK;
	if (AEInputKeyTriggered('3'))
		gAEGameStateNext = GS_TRANSFORM_DEMO;
}