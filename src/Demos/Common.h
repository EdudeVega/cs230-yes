#pragma once


// Simple model class
struct Model
{
	Vector2*	mVertexArray = NULL;
	u32			mVertexCount = 0;
};

// Creates a quad in the given model
Model* MakeQuad();

// Creates a cirlce in the given model
Model* MakeCircle();

// Draws a model in the given color
void DrawModel(Model *  model, const Transform2D & transform, unsigned color);

// Frees the vertex memory of a given quad
void FreeModel(Model * model);

// simple game object class
#define SHAPE_POINT 0
#define SHAPE_CIRCLE 1
#define SHAPE_RECTANGLE 2
#define SHAPE_ORIENTED_RECTANGLE 3
#define SHAPE_ELLIPSE 4

struct GameObject
{
	Model *		mpModel;
	Transform2D mTransform;
	u32			mCollisionShape;
	u32			mColor;
	bool		mAlive;
};

// Simple Object Manager
#define						MAX_OBJECT_NUM 2048
extern int					gObjectCount;
extern GameObject			gvAllObjects[MAX_OBJECT_NUM];

GameObject* NewGameObject(Vector2 pos, Vector2 sca, f32 rot, u32 shapeType, u32 color, Model* model);
void		DrawAllGameObjects();
void		DrawGameObject(GameObject* pObj);
void		DestroyObject(GameObject* obj);
void		ClearAllObjects();

// This function is used in the levels to handle quit, restart and level changing. Just call it a the top of your Update.
void GameFlow();