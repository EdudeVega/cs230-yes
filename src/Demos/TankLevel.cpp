
#include <AEEngine.h>
#include "..\Engine\Engine.h"
#include "GameStateList.h"
#include "Common.h"
#include "TankLevel.h"
#include "iostream"

// ----------------------------------------------------------------------------
// GLOBAL VARIABLES

// @PROVIDED
// Quad and circle model. Use these when creating your objects. 
Model* gQuad;
Model* gCircle;

// @TODO: Put your global variables here
GameObject* Body;
GameObject* Turret;
GameObject* mouseTurret;
GameObject* mouseGun;
GameObject* Gun;
GameObject* Enemiebody[4];
GameObject* EnemieTurret[4];
GameObject* EnemieGun[4];
GameObject* Bullet;
GameObject* EnemieBullet[4];
GameObject* Walls[2];
GameObject* Floors[2];
Transform2D localTurret;
Transform2D localGun;


Transform2D mouselocalTurret;
Transform2D mouselocalGun;

Transform2D EnemielocalTurret[4];
Transform2D EnemielocalGun[4];
bool Mouse = 0;
bool Ricochet = 0;
u32 deleteTime = 0;
bool shootTime = 0;
u32 EnemieDeleteTime[4] = {0,0,0,0};
bool EnemieShootTime[4] = {1,1,1,1};
// ----------------------------------------------------------------------------
// GAME STATE FUNCTIONS

void Update() {
	Vector2 Body_Ore;
	Body_Ore.FromAngle(Body->mTransform.mOrientation);

	Vector2 Gun_Ore;
	Gun_Ore.FromAngle(Gun->mTransform.mOrientation);

	if (AEInputKeyPressed('W')) {
		Body->mTransform.mPosition += Body_Ore * 2.5;
	}
	else if(AEInputKeyPressed('S')) {
		Body->mTransform.mPosition -= Body_Ore * 2.5;
	}
	if (AEInputKeyPressed('D')) {
		Body->mTransform.mOrientation = Body->mTransform.mOrientation + 1 * gAEFrameTime;
	}
	else if (AEInputKeyPressed('A')) {
		Body->mTransform.mOrientation = Body->mTransform.mOrientation  -1 * gAEFrameTime;
	}
	if (AEInputKeyPressed(0x27)) {
		localTurret.mOrientation = localTurret.mOrientation + 1 * gAEFrameTime;
	}
	else if (AEInputKeyPressed(0x25)) {
		localTurret.mOrientation = localTurret.mOrientation - 1 * gAEFrameTime;
	
	}
	if (!Mouse) {
		if (AEInputKeyTriggered(0x20) && Bullet && !shootTime) {
			Bullet->mAlive = 0;
			deleteTime = 0;
			shootTime = 1;
			Bullet = NewGameObject(Vector2(Gun->mTransform.mPosition.x + Gun->mTransform.mScale.x / 2 * cos(Gun->mTransform.mOrientation), (Gun->mTransform.mPosition.y + Gun->mTransform.mScale.x / 2 * sin(Gun->mTransform.mOrientation))), Vector2(15, 8), Gun->mTransform.mOrientation, SHAPE_RECTANGLE, AE_COLORS_YELLOW, gQuad);
			deleteTime++ * gAEFrameTime;

		}
		else if (AEInputKeyTriggered(0x20) && !shootTime) {
			deleteTime = 0;
			shootTime = 1;
			Bullet = NewGameObject(Vector2(Gun->mTransform.mPosition.x + Gun->mTransform.mScale.x / 2 * cos(Gun->mTransform.mOrientation), (Gun->mTransform.mPosition.y + Gun->mTransform.mScale.x / 2 * sin(Gun->mTransform.mOrientation))), Vector2(15, 8), Gun->mTransform.mOrientation, SHAPE_RECTANGLE, AE_COLORS_YELLOW, gQuad);
			deleteTime++ * gAEFrameTime;


		}
		if (Bullet) {

			Vector2 Bullet_Ore;
			Bullet_Ore.FromAngle(Bullet->mTransform.mOrientation);
			Bullet->mTransform.mPosition += Bullet_Ore * 4;
			deleteTime++ * gAEFrameTime;

		}
	}
	else {
		if (AEInputKeyTriggered(0x20) && Bullet && !shootTime) {
			Bullet->mAlive = 0;
			deleteTime = 0;
			shootTime = 1;
			Bullet = NewGameObject(Vector2(mouseGun->mTransform.mPosition.x + mouseGun->mTransform.mScale.x / 2 * cos(mouseGun->mTransform.mOrientation), (mouseGun->mTransform.mPosition.y + mouseGun->mTransform.mScale.x / 2 * sin(mouseGun->mTransform.mOrientation))), Vector2(15, 8), mouseGun->mTransform.mOrientation, SHAPE_RECTANGLE, AE_COLORS_YELLOW, gQuad);
			deleteTime++ * gAEFrameTime;

		}
		else if (AEInputKeyTriggered(0x20) && !shootTime) {
			deleteTime = 0;
			shootTime = 1;
			Bullet = NewGameObject(Vector2(mouseGun->mTransform.mPosition.x + mouseGun->mTransform.mScale.x / 2 * cos(mouseGun->mTransform.mOrientation), (mouseGun->mTransform.mPosition.y + mouseGun->mTransform.mScale.x / 2 * sin(mouseGun->mTransform.mOrientation))), Vector2(15, 8), mouseGun->mTransform.mOrientation, SHAPE_RECTANGLE, AE_COLORS_YELLOW, gQuad);
			deleteTime++ * gAEFrameTime;


		}
		if (Bullet) {

			Vector2 Bullet_Ore;
			Bullet_Ore.FromAngle(Bullet->mTransform.mOrientation);
			Bullet->mTransform.mPosition += Bullet_Ore * 4;
			deleteTime++ * gAEFrameTime;

		}
	}
	if (deleteTime == 200) {
		deleteTime = 0;
		DestroyObject(Bullet);
		shootTime = 0;
	}
	Vector2 mouse(gAEMousePosition - Body->mTransform.mPosition);
	mouselocalTurret.mOrientation =	mouse.GetAngle() -  localTurret.mOrientation - Body->mTransform.mOrientation ;
	mouseTurret->mTransform.mOrientation = gAEMousePosition.GetAngle();


	

	Turret->mTransform = Body->mTransform.Concat( localTurret);
	Gun->mTransform = Turret->mTransform.Concat(localGun);

	mouseTurret->mTransform = Gun->mTransform.Concat(mouselocalTurret);
	mouseGun->mTransform = mouseTurret->mTransform.Concat(mouselocalGun);

	std::cout << deleteTime;
	
}
void EnemyUpdate() {

	for (int i = 0; i < 4; i++) {
		Vector2 Gun_Enemie_Tank;
		if (Enemiebody[i]->mAlive) {
			if (Body->mTransform.mPosition.x > EnemieGun[i]->mTransform.mPosition.x) {
				Gun_Enemie_Tank = Body->mTransform.mPosition - EnemieGun[i]->mTransform.mPosition;
				EnemielocalTurret[i].mOrientation = Gun_Enemie_Tank.GetAngle();
			}

			else {
				Gun_Enemie_Tank = EnemieGun[i]->mTransform.mPosition - Body->mTransform.mPosition;
				EnemielocalTurret[i].mOrientation = Gun_Enemie_Tank.GetAngle();
			}

			EnemieTurret[i]->mTransform = Enemiebody[i]->mTransform.Concat(EnemielocalTurret[i]);
			EnemieGun[i]->mTransform = EnemieTurret[i]->mTransform.Concat(EnemielocalGun[i]);

			if (EnemieShootTime[i]) {
				EnemieDeleteTime[i] = 0;
				EnemieShootTime[i] = 0;
				EnemieBullet[i]->mTransform.mOrientation = EnemieGun[i]->mTransform.mOrientation;
				EnemieBullet[i]->mTransform.mPosition = Vector2(EnemieGun[i]->mTransform.mPosition.x + EnemieGun[i]->mTransform.mScale.x / 2 * cos(EnemieGun[i]->mTransform.mOrientation), EnemieGun[i]->mTransform.mPosition.y + EnemieGun[i]->mTransform.mScale.x / 2 * sin(EnemieGun[i]->mTransform.mOrientation));
				EnemieDeleteTime[i]++ * gAEFrameTime;
				EnemieBullet[i]->mAlive = 1;

			}
			if (EnemieBullet[i]->mAlive == 1) {
				Vector2 EnemieBullet_Ore;
				EnemieBullet_Ore.FromAngle(EnemieBullet[i]->mTransform.mOrientation);

				if (Body->mTransform.mPosition.x > EnemieGun[i]->mTransform.mPosition.x) {
					EnemieBullet[i]->mTransform.mPosition += EnemieBullet_Ore.NormalizeThis() * 4;
				}

				else {
					EnemieBullet[i]->mTransform.mPosition -= EnemieBullet_Ore.NormalizeThis() * 4;
				}

				
				EnemieDeleteTime[i]++ * gAEFrameTime;
			}
			if (EnemieDeleteTime[i] == 200) {
				EnemieShootTime[i] = 1;

			}

		}


	}
	
}

void Collisions() {


	if(Bullet)
		for (int i = 0; i < 4; i++) {
			if (Enemiebody[i]->mAlive) {
				if (StaticPointToStaticCircle(&Bullet->mTransform.mPosition, &Enemiebody[i]->mTransform.mPosition, Enemiebody[i]->mTransform.mScale.x / 2)) {
					Enemiebody[i]->mAlive = 0;
					EnemieGun[i]->mAlive = 0;
					EnemieTurret[i]->mAlive = 0;
					EnemieBullet[i]->mAlive = 0;
					deleteTime = 0;
					DestroyObject(Bullet);
					shootTime = 0;
				}

			}
			if(Ricochet)
				if (StaticPointToStaticRect(&Bullet->mTransform.mPosition, &Walls[i]->mTransform.mPosition, Walls[i]->mTransform.mScale.x, Walls[i]->mTransform.mScale.y)) 
					Bullet->mTransform.mOrientation = Bullet->mTransform.mOrientation - 180;

			
			
		}
	for (int j = 0; j < 4; j++) {

		if (EnemieBullet[j]->mAlive)
			for (int i = 0; i < 4; i++) {
				if(Ricochet)
					if (StaticPointToStaticRect(&EnemieBullet[j]->mTransform.mPosition, &Walls[i]->mTransform.mPosition, Walls[i]->mTransform.mScale.x, Walls[i]->mTransform.mScale.y))
						EnemieBullet[j]->mTransform.mOrientation = EnemieBullet[j]->mTransform.mOrientation - 180;

				if (StaticPointToOrientedRect(&EnemieBullet[j]->mTransform.mPosition, &Body->mTransform.mPosition, Body->mTransform.mScale.x, Body->mTransform.mScale.y, Body->mTransform.mOrientation)) {
					EnemieBullet[i]->mAlive = 0;
					EnemieShootTime[i] = 1;
					gAEGameStateNext = AE_GS_RESTART;

				}


					
			}
	}

}
//@PROVIDED
void TankLevel_Load()
{
	// create quad model
	gQuad = MakeQuad();

	// create circle model
	gCircle = MakeCircle();
}
//@PROVIDED
void TankLevel_Unload()
{
	FreeModel(gQuad);
	FreeModel(gCircle);
}
//@PROVIDED
void TankLevel_Free()
{
	// remove all alive objects and reset object count. 
	ClearAllObjects();
}
//@TODO
void TankLevel_Init()
{

	// @TODO: Create your game objects here. Example: Create a simple rectangle
	Body = NewGameObject(Vector2(0, 0), Vector2(100, 50), 0.0f, SHAPE_RECTANGLE, AE_COLORS_WHITE, gQuad);
	Turret = NewGameObject(Vector2(0, 0), Vector2(20, 20), 0.0f, SHAPE_CIRCLE, AE_COLORS_WHITE, gCircle);
	Gun = NewGameObject(Vector2(47.5, 0), Vector2(75, 15), 0.0f, SHAPE_RECTANGLE, AE_COLORS_WHITE, gQuad);
	mouseTurret = NewGameObject(Vector2(95, 0), Vector2(20, 20), 0.0f, SHAPE_CIRCLE, AE_COLORS_WHITE, gCircle);
	mouseGun = NewGameObject(Vector2(143, 0), Vector2(75, 15), 0.0f, SHAPE_RECTANGLE, AE_COLORS_WHITE, gQuad);

	Enemiebody[0] = NewGameObject(Vector2(-350, 250), Vector2(50, 50), 0.0f, SHAPE_RECTANGLE, AE_COLORS_RED, gCircle);
	Enemiebody[1] = NewGameObject(Vector2(-350, -250), Vector2(50, 50), 0.0f, SHAPE_RECTANGLE, AE_COLORS_RED, gCircle);
	Enemiebody[2] = NewGameObject(Vector2(350, 250), Vector2(50, 50), 0.0f, SHAPE_RECTANGLE, AE_COLORS_RED, gCircle);
	Enemiebody[3] = NewGameObject(Vector2(350, -250), Vector2(50, 50), 0.0f, SHAPE_RECTANGLE, AE_COLORS_RED, gCircle);

	EnemieTurret[0] = NewGameObject(Vector2(-350, 250), Vector2(15, 15), 0.0f, SHAPE_CIRCLE, AE_COLORS_RED, gCircle);
	EnemieTurret[1] = NewGameObject(Vector2(-350, -250), Vector2(15, 15), 0.0f, SHAPE_CIRCLE, AE_COLORS_RED, gCircle);
	EnemieTurret[2] = NewGameObject(Vector2(350, 250), Vector2(15, 15), 0.0f, SHAPE_CIRCLE, AE_COLORS_RED, gCircle);
	EnemieTurret[3] = NewGameObject(Vector2(350, -250), Vector2(15, 15), 0.0f, SHAPE_CIRCLE, AE_COLORS_RED, gCircle);

	EnemieGun[0] = NewGameObject(Vector2(-325, 250), Vector2(35, 8), 0.0f, SHAPE_RECTANGLE, AE_COLORS_RED, gQuad);
	EnemieGun[1] = NewGameObject(Vector2(-325, -250), Vector2(35, 8), 0.0f, SHAPE_RECTANGLE, AE_COLORS_RED, gQuad);
	EnemieGun[2] = NewGameObject(Vector2(325, 250), Vector2(35, 8), 0.0f, SHAPE_RECTANGLE, AE_COLORS_RED, gQuad);
	EnemieGun[3] = NewGameObject(Vector2(325, -250), Vector2(35, 8), 0.0f, SHAPE_RECTANGLE, AE_COLORS_RED, gQuad);



	Walls[0] = NewGameObject(Vector2(-400, 0), Vector2(50, 1000), 0.0f, SHAPE_RECTANGLE, AE_COLORS_WHITE, gQuad);
	Walls[1] = NewGameObject(Vector2(0, 350), Vector2(1000, 50), 0.0f, SHAPE_RECTANGLE, AE_COLORS_WHITE, gQuad);
	Walls[2] = NewGameObject(Vector2(400, 0), Vector2(50, 1000), 0.0f, SHAPE_RECTANGLE, AE_COLORS_WHITE, gQuad);
	Walls[3] = NewGameObject(Vector2(0, -350), Vector2(1000, 50), 0.0f, SHAPE_RECTANGLE, AE_COLORS_WHITE, gQuad);



	Mouse = 0;
	Ricochet = 0;
	deleteTime = 0;
	shootTime = 0;
	for (int i = 0; i < 4; i++) {
	EnemieDeleteTime[i] = 0;
	EnemieShootTime[i] = 1;
	}

	mouseGun->mAlive = 0;
	mouseTurret->mAlive = 0;
	localTurret = Body->mTransform.InverseConcat(Turret->mTransform);
	localGun = Turret->mTransform.InverseConcat(Gun->mTransform);
	mouselocalTurret = Gun->mTransform.InverseConcat(mouseTurret->mTransform);
	mouselocalGun = mouseTurret->mTransform.InverseConcat(mouseGun->mTransform);

	for (int i = 0; i < 4; i++) {
		EnemielocalTurret[i] = Enemiebody[i]->mTransform.InverseConcat(EnemieTurret[i]->mTransform);
		EnemielocalGun[i] = EnemieTurret[i]->mTransform.InverseConcat(EnemieGun[i]->mTransform);
		
		EnemieBullet[i] = NewGameObject(Vector2(Gun->mTransform.mPosition.x + Gun->mTransform.mScale.x / 2 * cos(Gun->mTransform.mOrientation), (Gun->mTransform.mPosition.y + Gun->mTransform.mScale.x / 2 * sin(Gun->mTransform.mOrientation))), Vector2(15, 8), Gun->mTransform.mOrientation, SHAPE_RECTANGLE, AE_COLORS_YELLOW, gQuad);
		EnemieBullet[i]->mAlive = 0;
	}
}
//@TODO
void TankLevel_Update()
{	
	Collisions();
	Update();
	EnemyUpdate();
	if (AEInputKeyPressed('R')){
		if (Ricochet) Ricochet = 0;
		else Ricochet = 1;
	}
	if (AEInputKeyPressed('T')) {
		if (Mouse) {
			mouseGun->mAlive = 0;
			mouseTurret->mAlive = 0;
			Mouse = 0;
		}

		else {
			mouseGun->mAlive = 1;
			mouseTurret->mAlive = 1;
			Mouse = 1;
		}

	}
	
	GameFlow();
}
//@TODO
void TankLevel_Render()
{
	// Print Debug
	AEGfxPrint(0, 0, 0xFFFFFFFF, "LEVEL 2: TANKS");

	DrawAllGameObjects();
}
