/*!
*	\file		TankLevel.h
*	\brief		Contains the code for the Tank demo.
*	\author		Thomas Komair - tkomair@digipen.edu
*	\date		12/02/2018
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

#ifndef TANK_H_
#define TANK_H_

void TankLevel_Load();
void TankLevel_Unload();
void TankLevel_Init();
void TankLevel_Free();
void TankLevel_Update();
void TankLevel_Render();


#endif