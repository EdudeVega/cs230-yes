/*!
*	\file		TestStaticCollisions.h
*	\brief		Contains the code for the static collision tests demo.
*	\details	The following tests are tested:
					- Point-Line classification using mouse
					- Point-Circle collision using mouse picking
					- Point-Rectangle collision using mouse picking
					- Circle Circle collision when moving circle with mouse
					- Circle Rectangle Collision when moving rectangle with mouse


*	\author		Thomas Komair - tkomair@digipen.edu
*	\date		12/02/2018
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

// ----------------------------------------------------------------------------
// INCLUDES
#include <AEEngine.h>
#include "..\Engine\Engine.h"
#include "TestStaticCollisions.h"
#include "Common.h"

// ----------------------------------------------------------------------------
// GLOBALS

GameObject*			pPickedObject = NULL; 
Model*				gQuadModel;
Model*				gCircleModel;

// ----------------------------------------------------------------------------
// HELPER FUNCTIONS
void KeyboardInput();
void PickObjects();
void DetectCollisions();
void RenderGOWithExtents(GameObject * pObj, bool renderExtents);

// ----------------------------------------------------------------------------
// GAME STATE FUNCTIONS

void TestStaticCollisions_Load()
{
	gQuadModel = MakeQuad();
	gCircleModel = MakeCircle();
}
void TestStaticCollisions_Init()
{
	pPickedObject = NULL;
	gObjectCount = 0;
}
void TestStaticCollisions_Update()
{
	GameFlow();

	// reset color of all objects to white
	for (int i = 0; i < gObjectCount; ++i){
		gvAllObjects[i].mColor = 0xFFFFFFFF;
	}

	// Keyboard
	KeyboardInput();

	// Pick objects
	PickObjects();

	// Do Collision
	DetectCollisions();
}
void TestStaticCollisions_Render()
{
	// Disable modulation color
	AEGfxEnableModulationColor(false);

	for (int i = 0; i < gObjectCount; ++i)
	{
		RenderGOWithExtents(gvAllObjects+i, true);
	}

	// Print Debug
	AEGfxPrint(0, 0, 0xFFFFFFFF, "LEVEL 1: TEST STATIC COLLISIONS");
}
void TestStaticCollisions_Free()
{
	ClearAllObjects();
}
void TestStaticCollisions_Unload()
{
}


// ----------------------------------------------------------------------------
// HELPER FUNCTIONS
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------

void KeyboardInput()
{
	if (AEInputKeyTriggered('C'))
	{
		// Make a random circle
		float sx = AERandFloat(30, 60);
		NewGameObject(
			Vector2::Random(-350, 350, -250, 250),	// position
			Vector2(sx, sx),						// scale
			0.0f,									// orientation
			SHAPE_CIRCLE,							// collision shape 
			AE_COLORS_WHITE,						// color
			gCircleModel							// circle model
		);
	}
	if (AEInputKeyTriggered('V'))
	{
		// make a random AABB
		NewGameObject(
			Vector2::Random(-350, 350, -250, 250),	// position
			Vector2::Random(100, 160, 100, 160),	// scale
			0.0f,									// orientation
			SHAPE_RECTANGLE,						// collision shape 
			AE_COLORS_WHITE,						// color
			gQuadModel								// quad model
		);
	}
	if (AEInputKeyTriggered('B'))
	{
		// make a random OBB
		NewGameObject(
			Vector2::Random(-350, 350, -250, 250),	// position
			Vector2::Random(100, 160, 100, 160),	// scale
			AERandFloat(0, TWO_PI),					// orientation
			SHAPE_ORIENTED_RECTANGLE,				// collision shape 
			AE_COLORS_WHITE,						// color
			gQuadModel								// quad model
		);
	}
}

// ----------------------------------------------------------------------------

void PickObjects()
{
	if (AEInputMouseTriggered(AE_MOUSE_LEFT) && NULL == pPickedObject)
	{
		// iterate to pick
		Vector2 mousePos = { gAEMousePosition.x, gAEMousePosition.y };
		Vector2 pos;

		for (int i = 0; i < gObjectCount; ++i)
		{
			GameObject * pObj = gvAllObjects + i;
			pos = pObj->mTransform.mPosition;

			if (pObj->mCollisionShape == SHAPE_CIRCLE && StaticPointToStaticCircle(&mousePos,
				&pos,
				pObj->mTransform.mScale.x / 2.0f))
			{
				pObj->mColor = 0xFF00FF00;
				pPickedObject = pObj;
			}
			else if (pObj->mCollisionShape == SHAPE_RECTANGLE && StaticPointToStaticRect(&mousePos,
				&pos,
				pObj->mTransform.mScale.x,
				pObj->mTransform.mScale.y))
			{
				pObj->mColor = 0xFF00FF00;
				pPickedObject = pObj;
			}
			else if (pObj->mCollisionShape == SHAPE_ORIENTED_RECTANGLE && StaticPointToOrientedRect(&mousePos,
				&pos,
				pObj->mTransform.mScale.x,
				pObj->mTransform.mScale.y,
				pObj->mTransform.mOrientation))
			{
				pObj->mColor = 0xFF00FF00;
				pPickedObject = pObj;
			}
		}
	}
	else if (AEInputMousePressed(AE_MOUSE_LEFT) && NULL != pPickedObject)
	{
		pPickedObject->mTransform.mPosition = from_ae_vec2(gAEMousePosition);
		pPickedObject->mColor = 0xFF00FF00;

		if (pPickedObject->mCollisionShape == SHAPE_ORIENTED_RECTANGLE)
		{
			if (AEInputKeyPressed('A'))
				pPickedObject->mTransform.mOrientation += AEDegToRad(1.0f);
			if (AEInputKeyPressed('D'))
				pPickedObject->mTransform.mOrientation -= AEDegToRad(1.0f);
		}
	}
	else if (!AEInputMousePressed(AE_MOUSE_LEFT) && NULL != pPickedObject){
		pPickedObject->mColor = 0xFFFFFFFF;
		pPickedObject = NULL;
	}
		

}

// ----------------------------------------------------------------------------

void CollideCircles(GameObject * pObj1, GameObject *pObj2)
{
	Vector2 pos1, pos2;
	Vector2 sca1, sca2;
	pos1 = pObj1->mTransform.mPosition;
	sca1 = pObj1->mTransform.mScale;
	pos2 = pObj2->mTransform.mPosition;
	sca2 = pObj2->mTransform.mScale;

	if (StaticCircleToStaticCircle(&pos1, sca1.x / 2.0f, &pos2, sca2.x / 2.0f))
	{
		pObj1->mColor = pObj2->mColor = 0xFFFF0000;
	}
}
void CollideRectangles(GameObject * pObj1, GameObject *pObj2)
{
	Vector2 pos1, pos2;
	Vector2 sca1, sca2;
	pos1 = pObj1->mTransform.mPosition;
	sca1 = pObj1->mTransform.mScale;
	pos2 = pObj2->mTransform.mPosition;
	sca2 = pObj2->mTransform.mScale;

	if (StaticRectToStaticRect( &pos1, sca1.x, sca1.y, 
								&pos2, sca2.x, sca2.y))
	{
		pObj1->mColor = pObj2->mColor = 0xFFFF0000;
	}
}
void CollideCircleRectangle(GameObject * circle, GameObject *rectangle)
{
	Vector2 pos1, pos2;
	Vector2 sca1, sca2;
	pos1 = circle->mTransform.mPosition;
	sca1 = circle->mTransform.mScale;
	pos2 = rectangle->mTransform.mPosition;
	sca2 = rectangle->mTransform.mScale;

	if (StaticRectToStaticCirlce(&pos2, sca2.x, sca2.y,
		&pos1, sca1.x / 2.0f))
	{
		circle->mColor = rectangle->mColor = 0xFFFF00FF;
	}
}
void CollideCircleOrientedRectangle(GameObject * circle, GameObject *rectangle)
{
	Vector2 pos1, pos2;
	Vector2 sca1, sca2;
	float angle;
	pos1 = circle->mTransform.mPosition;
	sca1 = circle->mTransform.mScale;
	pos2 = rectangle->mTransform.mPosition;
	sca2 = rectangle->mTransform.mScale;
	angle = rectangle->mTransform.mOrientation;

	if (OrientedRectToStaticCirlce(&pos2, sca2.x, sca2.y, angle,
		&pos1, sca1.x / 2.0f))
	{
		circle->mColor = rectangle->mColor = 0xFFFF00FF;
	}
}

// ----------------------------------------------------------------------------

void DetectCollisions()
{
	/*
	Test Collisions between all the circles together
	Test Collisions between all the rectangles together
	Test Collisions between all the rectangles and circles
	*/

	for (int i = 0; i < gObjectCount; ++i)
	{
		GameObject * pObj1 = gvAllObjects+i;

		for (int j = i+1; j < gObjectCount; ++j)
		{
			GameObject * pObj2 = gvAllObjects+j;

			if (pObj1->mCollisionShape == SHAPE_CIRCLE && pObj2->mCollisionShape == SHAPE_CIRCLE)
			{
				CollideCircles(pObj1, pObj2);
			}
			else if (pObj1->mCollisionShape == SHAPE_RECTANGLE && pObj2->mCollisionShape == SHAPE_RECTANGLE)
			{
				CollideRectangles(pObj1, pObj2);
			}
			else if (pObj1->mCollisionShape == SHAPE_CIRCLE && pObj2->mCollisionShape == SHAPE_RECTANGLE)
			{
				CollideCircleRectangle(pObj1, pObj2);
			}
			else if (pObj2->mCollisionShape == SHAPE_CIRCLE && pObj1->mCollisionShape == SHAPE_RECTANGLE)
			{
				CollideCircleRectangle(pObj2, pObj1);
			}
			else if (pObj1->mCollisionShape == SHAPE_CIRCLE && pObj2->mCollisionShape == SHAPE_ORIENTED_RECTANGLE)
			{
				CollideCircleOrientedRectangle(pObj1, pObj2);
			}
			else if (pObj2->mCollisionShape == SHAPE_CIRCLE && pObj1->mCollisionShape == SHAPE_ORIENTED_RECTANGLE)
			{
				CollideCircleOrientedRectangle(pObj2, pObj1);
			}
		}
	}
}

// ----------------------------------------------------------------------------

void RenderGOWithExtents(GameObject * pObj, bool renderExtents)
{
	DrawGameObject(pObj);

	if (renderExtents)
	{
		// Identity matrix for line drawing
		AEGfxEnableModulationColor(false);
		AEMtx33 ae_world = pObj->mTransform.GetMatrix();
		AEGfxSetTransform(&ae_world);

		// draw the half width extent
		AEGfxLine(0, 0, 0, pObj->mColor, 0.5f, 0, 0, pObj->mColor);
		// draw the half height extent
		AEGfxLine(0, 0, 0, pObj->mColor, 0.0f, 0.5f, 0, pObj->mColor);

		// draw the lines now
		AEGfxFlush();
	}
}