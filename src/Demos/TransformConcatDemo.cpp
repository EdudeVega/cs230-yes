// ---------------------------------------------------------------------------
// Project Name		:	Alpha Engine
// File Name		:	Level.cpp
// Author			:	Thomas Komair
// Creation Date	:	2013/04/26
// Purpose			:	Implementation of demo level game states functions.
// History			:
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// INCLUDES
#include "TransformConcatDemo.h"
#include <AEEngine.h>
#include "..\Engine\Math\MyMath.h"
#include "..\Engine\Utils\MeshCreation.h"
#include "Common.h"


// ---------------------------------------------------------------------------
// GLOBAL VARIABLES AND DEFINITIONS

// LAB 3 COMMONS - These need to be defined somewhere (only once). 
AEGfxTriList *	gpQuad = NULL;

u32 gMode = 0;	// 0 -> using transform
				// 1 -> using matrix concatenation
u32 gControl = 0;	// 0 -> controlling parent
					// 1 -> controlling child

					// These would be the data for two objects who are relative to one another

Transform2D gTBody, gTShoulder, *gpControlled = &gTBody;
u32 child_color = gControl ? 0xFFFF0000 : 0xFFFFFFFF;
u32 parent_color = !gControl ? 0xFFFF0000 : 0xFFFFFFFF;

// ---------------------------------------------------------------------------
// HELPER FUNCTIONS DECLARATIONS
void RenderObject1();
void RenderObject2();
void PrintTransform(int x, int y, const char * title, u32 color, Transform2D & transform);


// ---------------------------------------------------------------------------
// GAME STATE FUNCTIONS
void TransformConcatDemo_Load()
{
	// Create the Quad Mesh of WHITE color
	gpQuad = MakeQuad(0xFFFFFFFF);
}
void TransformConcatDemo_Init()
{
	// Set the background color to black.
	AEGfxSetClearColor(0xFF000000);

	// the parent transform
	gTBody.mOrientation = 0.0f;
	gTBody.mScale.x = 60.0f;
	gTBody.mScale.y = 60.0f;
	gTBody.mPosition.x = 0;
	gTBody.mPosition.y = 0;

	// the child transform
	gTShoulder.mOrientation = 0.0f;
	gTShoulder.mScale.x = 1.0f;
	gTShoulder.mScale.y = 1.0f;
	gTShoulder.mPosition.x = 2.0f;
	gTShoulder.mPosition.y = 0.0f;

	gpControlled = &gTBody;

	gControl = 0;
	gMode = 0;
	child_color = 0xFFFFFFFF;
	parent_color = 0xFFFF0000;
}
void TransformConcatDemo_Update()
{
	GameFlow();
	bool shift = AEInputKeyPressed(AE_KEY_SHIFT) != 0;
	f32 inputScale = shift ? 5.0f : 1.0f;
	if (AEInputKeyTriggered('R')) gAEGameStateNext = AE_GS_RESTART;
	if (AEInputKeyTriggered('M'))gMode = ++gMode % 2;

	if (AEInputKeyTriggered('C')) // controlling child or parent
	{
		// toggle control
		gControl = ++gControl % 2;

		// set correct pointer for movement input
		gpControlled = gControl == 0 ? &gTBody : &gTShoulder;

		// update colors
		child_color = gControl ? 0xFFFF0000 : 0xFFFFFFFF;
		parent_color = !gControl ? 0xFFFF0000 : 0xFFFFFFFF;
	}

	// MOVEMENT INPUT
	/////

	// Position
	if (AEInputKeyPressed('A'))
	{
		gpControlled->mPosition.x -= 1.f * inputScale;
	}
	if (AEInputKeyPressed('D'))
	{
		gpControlled->mPosition.x += 1.f * inputScale;
	}
	if (AEInputKeyPressed('S'))
	{
		gpControlled->mPosition.y -= 1.f * inputScale;
	}
	if (AEInputKeyPressed('W'))
	{
		gpControlled->mPosition.y += 1.f * inputScale;
	}

	// Scale
	if (AEInputKeyPressed('J'))
	{
		gpControlled->mScale.x -= 0.1f * inputScale;
	}
	if (AEInputKeyPressed('L'))
	{
		gpControlled->mScale.x += 0.1f * inputScale;
	}
	if (AEInputKeyPressed('K'))
	{
		gpControlled->mScale.y -= 0.1f * inputScale;
	}
	if (AEInputKeyPressed('I'))
	{
		gpControlled->mScale.y += 0.1f * inputScale;
	}

	// Rotation
	if (AEInputKeyPressed('E'))
	{
		gpControlled->mOrientation -= AEDegToRad(1.0f * inputScale);
	}
	if (AEInputKeyPressed('Q'))
	{
		gpControlled->mOrientation += AEDegToRad(1.0f * inputScale);
	}
}
void TransformConcatDemo_Draw()
{
	AEGfxPrint(2, 2, 0xFFFFFFFF, "---TRANSOFRM CONCATENATION DEMO---");

	if (!gMode)
	{
		AEGfxPrint(2, 2, 0xFFFFFFFF, "\nUSING TRANSFORMATION");
		RenderObject1();
	}
	else
	{
		AEGfxPrint(2, 2, 0xFFFFFFFF, "\nUSING MATRIX CONCATENATION");
		RenderObject2();
	}

	if (!gControl)
		AEGfxPrint(2, 2, 0xFFFFFFFF, "\n\nCONTROLLING PARENT");
	else
		AEGfxPrint(2, 2, 0xFFFFFFFF, "\n\nCONTROLLING CHILD");

	PrintTransform(340, 2, "PARENT", parent_color, gTBody);
	PrintTransform(500, 2, "CHILD (LOCAL)", child_color, gTShoulder);
	PrintTransform(650, 2, "CHILD (WORLD)", AE_COLORS_YELLOW, gTBody * gTShoulder);

	Transform2D concat = gTBody * gTShoulder;
	Transform2D local = gTBody.InverseConcat(concat);
	PrintTransform(500, 60, "CHILD (REBUILT)", child_color, local);

}
void TransformConcatDemo_Free()
{
}
void TransformConcatDemo_Unload()
{
	AEGfxTriFree(gpQuad);
}

// ---------------------------------------------------------------------------
// HELPER FUNCTIONS IMPLEMENTATIONS
void PrintTransform(int x, int y, const char * title, u32 color, Transform2D & tr)
{
	static char tmpBuffer[512]; // static tmp string to use with sprintf_s
	sprintf_s(tmpBuffer, "%s\n\
Pos = (%.2f,%.2f)\n\
Sca = (%.2f,%.2f)\n\
Rot = %.0f degs\n", title, tr.mPosition.x, tr.mPosition.y, tr.mScale.x, tr.mScale.y, AERadToDeg(tr.mOrientation));

	AEGfxPrint(x, y, color, tmpBuffer);
}
void RenderObject1()
{
	// enable modulation color
	AEGfxEnableModulationColor(true);

	// auxiliary matrix
	Matrix33 resMtx;
	Transform2D resT, resTC;

	// render the parent
	resMtx = gTBody.GetMatrix();

	// Convert to AEMtx33 format
	AEMtx33 proxy;
	for (int i = 0; i < 9; ++i)
		proxy.v[i] = resMtx.v[i];

	// Set the transform matrix
	AEGfxSetModulationColor(parent_color);
	AEGfxSetTransform(&proxy);
	AEGfxTriDraw(gpQuad);
	AEGfxLine(0, 0, 0, 0xFF101000, 0.5f, 0, 0, 0xFF101000);
	AEGfxLine(0, 0, 0, 0xFF101000, 0, 0.5f, 0, 0xFF101000);
	AEGfxFlush();

	// here we will use the concatenation of both transforms to render the objects
	resT = gTBody * gTShoulder;
	resMtx = resT.GetMatrix();

	// Convert to AEMtx33 format
	for (int i = 0; i < 9; ++i)
		proxy.v[i] = resMtx.v[i];

	// Set the transform matrix
	AEGfxSetModulationColor(child_color);
	AEGfxSetTransform(&proxy);
	AEGfxTriDraw(gpQuad);
	AEGfxLine(0, 0, 0, 0xFF101000, 0.5f, 0, 0, 0xFF101000);
	AEGfxLine(0, 0, 0, 0xFF101000, 0, 0.5f, 0, 0xFF101000);
	AEGfxFlush();
}
void RenderObject2()
{
	// enable modulation color
	AEGfxEnableModulationColor(true);

	// Matrices
	Matrix33 worldP, worldC, resMtx;
	Matrix33 trMtx1, trMtx2, scMtx1, scMtx2, rotMtx1, rotMtx2;

	trMtx1 = Matrix33::Translate(gTBody.mPosition.x, gTBody.mPosition.y);
	scMtx1 = Matrix33::Scale(gTBody.mScale.x, gTBody.mScale.y);
	rotMtx1 = Matrix33::RotRad(gTBody.mOrientation);

	trMtx2 = Matrix33::Translate(gTShoulder.mPosition.x, gTShoulder.mPosition.y);
	scMtx2 = Matrix33::Scale(gTShoulder.mScale.x, gTShoulder.mScale.y);
	rotMtx2 = Matrix33::RotRad(gTShoulder.mOrientation);

	// Concatenate
	worldP = trMtx1 * rotMtx1 * scMtx1;
	worldC = trMtx2 * rotMtx2 * scMtx2;
	resMtx = worldP * worldC;

	AEMtx33 proxy;
	for (int i = 0; i < 9; ++i)
		proxy.v[i] = resMtx.v[i];

	// Set the transform matrix
	AEGfxSetModulationColor(child_color);
	AEGfxSetTransform(&proxy);
	AEGfxTriDraw(gpQuad);
	AEGfxLine(0, 0, 0, 0xFF101000, 0.5f, 0, 0, 0xFF101000);
	AEGfxLine(0, 0, 0, 0xFF101000, 0, 0.5f, 0, 0xFF101000);
	AEGfxFlush();

	// render the parent
	for (int i = 0; i < 9; ++i)
		proxy.v[i] = worldP.v[i];
	// Set the transform matrix
	AEGfxSetModulationColor(parent_color);
	AEGfxSetTransform(&proxy);
	AEGfxTriDraw(gpQuad);
	AEGfxLine(0, 0, 0, 0xFF101000, 0.5f, 0, 0, 0xFF101000);
	AEGfxLine(0, 0, 0, 0xFF101000, 0, 0.5f, 0, 0xFF101000);
	AEGfxFlush();

}
