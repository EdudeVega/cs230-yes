/*!
*	\file		TestStaticCollisions.h
*	\brief		Contains the code for the static collision tests demo.
*	\author		Thomas Komair - tkomair@digipen.edu
*	\date		12/02/2018
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/

#ifndef TEST_STATIC_H_
#define TEST_STATIC_H_

void TestStaticCollisions_Load();
void TestStaticCollisions_Init();
void TestStaticCollisions_Update();
void TestStaticCollisions_Render();
void TestStaticCollisions_Free();
void TestStaticCollisions_Unload();

#endif